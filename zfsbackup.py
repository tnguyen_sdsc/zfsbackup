#!/bin/python3

## What this script does:
## 1. Creates a local snapshot if none is present and sends initial to remote host
## 2. If local snapshot is present but remote doesn't exist, sends first snapshot to remote host
## 3. If there is only 1 snapshot in local, create a 2nd and send incremental to remote
## 4. If there are multiple snapshots in local, send incremental to remote until caught up

## Author: Thanh Nguyen
## Email: tnguyen@sdsc.edu
## Date: 20240516
## Version: 0.1

## These are the variables that should be set
local_dataset = "datastore/data"
remote_dataset = "zfs_backup/data"
remote_server = "induction-target.sdsc.edu"
local_keep_days = "7" 
remote_keep_days = "90" 
minimal_local_to_keep = "7" # Must be greater than 2
minimal_remote_to_keep = "90" # Must be greater than 2
zfs = "/usr/sbin/zfs"
ssh = "/usr/bin/ssh"

from datetime import date
import subprocess
import time
import datetime

## Gets snapshots from local host and put in array
## Returns array of snapshots
def get_local_snapshots(local_dataset):
    snapshots = []
    # Run 'zfs list -t snap -o name' command and capture the output
    output = subprocess.check_output([zfs, 'list', '-t', 'snap', '-o', 'name', '-s', 'creation']).decode('utf-8')
    # Split the output into lines and iterate over them
    for line in output.split('\n'):
        # Extract snapshot name from the line
        snapshot_name = line.strip()
        # Check if the snapshot belongs to the specified dataset
        if snapshot_name.startswith(local_dataset + '@'):
            snapshots.append(snapshot_name)
    return snapshots

def get_remote_snapshots(remote_dataset, remote_server):
    snapshots = []
    # Run 'zfs list -t snap -o name' command and capture the output
    output = subprocess.check_output([ssh, remote_server, zfs, 'list', '-t', 'snap', '-o', 'name', '-s', 'creation']).decode('utf-8')
    # Split the output into lines and iterate over them
    for line in output.split('\n'):
        # Extract snapshot name from the line
        snapshot_name = line.strip()
        # Check if the snapshot belongs to the specified dataset
        if snapshot_name.startswith(remote_dataset + '@'):
            snapshots.append(snapshot_name)
    return snapshots

def return_last_matched(local_snaps, remote_snaps):
    local_days = [item.split('@')[-1] for item in local_snaps]
    remote_days = [item.split('@')[-1] for item in remote_snaps]
    #Find last match
    last_match = None
    for item in reversed(remote_days):
        if item in local_days:
            last_match = item
            break
    print("Last match:", last_match)
    return last_match

def return_first_unmatched(local_snaps, remote_snaps):
    local_days = [item.split('@')[-1] for item in local_snaps]
    remote_days = [item.split('@')[-1] for item in remote_snaps]
    snaps_not_in_remote_snaps = [item for item in local_days if item not in remote_days]
    print("First item not matched:", snaps_not_in_remote_snaps[0])
    return snaps_not_in_remote_snaps[0]

def send_incremental_snapshot(local_dataset, remote_dataset, remote_server, previous, current):
    send = subprocess.Popen([zfs, 'send', '-i', local_dataset + '@' + previous, local_dataset + '@' + current], stdout=subprocess.PIPE)
    output = subprocess.check_output([ssh, remote_server, zfs, 'receive', '-F', '-u', '-v', remote_dataset], stdin=send.stdout)
    send.wait()

def fdate():
    today = date.today()
    return today.strftime('%Y%m%d')

def backup():
    while True:
        remote_snapshots_array = get_remote_snapshots(remote_dataset, remote_server)
        local_snapshots_array = get_local_snapshots(local_dataset)
        last_matched = return_last_matched(local_snapshots_array, remote_snapshots_array)
        today_date = fdate()
        if not local_snapshots_array: 
            print("Creating first snapshot on local host")
            output = subprocess.check_output([zfs, 'snap', local_dataset + '@' + fdate()])
            print("Sending first snapshot over to remote host")
            send = subprocess.Popen([zfs, 'send', '-v', local_dataset + '@' + fdate()], stdout=subprocess.PIPE)
            sendout = subprocess.check_output([ssh, remote_server, zfs, 'receive', '-F', '-u', '-v', remote_dataset], stdin=send.stdout)
            send.wait()
            remote_snapshots_array = get_remote_snapshots(remote_dataset, remote_server)
        elif len(local_snapshots_array) >= 1 and len(remote_snapshots_array) == 0:
            print("Local snapshot is present but none found on remote")
            last_local_snapshot = local_snapshots_array[-1].split('@')[-1]
            snapdate = local_snapshots_array[-1].split('@')[-1]
            print("Sending ALL snapshot to remote host")
            send = subprocess.Popen([zfs, 'send', '-R', local_dataset + '@' + snapdate], stdout=subprocess.PIPE)
            sendout = subprocess.check_output([ssh, remote_server, zfs, 'receive', '-F', '-u', '-v', remote_dataset], stdin=send.stdout)
            send.wait()
            time.sleep(1)
        elif local_snapshots_array[-1].split('@')[-1] != today_date:
            print("last local snap " + local_snapshots_array[-1])
            print("today's date " + today_date)
            print("Need to create snapshot for today and send incremental")
            output = subprocess.check_output([zfs, 'snap', local_dataset + '@' + fdate()])
            previous = local_snapshots_array[-1].split('@')[-1]
            current =  today_date
            send_incremental_snapshot(local_dataset, remote_dataset, remote_server, previous, current)
        else:
            try:
                first_unmatched = return_first_unmatched(local_snapshots_array, remote_snapshots_array)
            except:
                print("All snapshots are sent to " + remote_server + "!!")
                break
            else:
                print(f"Sending snaps " + str(last_matched) + " " + str(first_unmatched))
                send_incremental_snapshot(local_dataset, remote_dataset, remote_server, last_matched, first_unmatched)
                #send_incremental_snapshot(local_dataset, remote_dataset, remote_server, last_matched, last_local_snapshot)
                time.sleep(1)

def get_local_snapshots_epoch(snapshot):
    # Run 'zfs list -t -p snap -o name' command and capture the output
    snapshot_epoch = subprocess.check_output([zfs, 'list', '-p', '-t', 'snap', '-o', 'creation', '-s', 'creation']).decode('utf-8')


def local_prune(local_keep_days, minimal_local_to_keep):
    prune_epoch = (datetime.datetime.today() - datetime.timedelta(days=int(local_keep_days))).strftime("%s")
    local_snapshots = get_local_snapshots(local_dataset)
    print("Enter pruning for local system")
    for item in (local_snapshots):
        local_snapshots = get_local_snapshots(local_dataset)
        if len(local_snapshots) >= int(minimal_local_to_keep):
            snapshot_epoch = subprocess.check_output([zfs, 'list', '-H', '-p', '-t', 'snap', item, '-o', 'creation']).decode('utf-8')
            if snapshot_epoch < prune_epoch:
                print("Pruning loop " + item)
                prune = subprocess.check_output([zfs, 'destroy', item]).decode('utf-8')
#            else:
#                print("Finished pruning")
        else:
            print("No pruning since there are " + minimal_local_to_keep + " or less snapshots")
            break


def remote_prune(remote_keep_days, minimal_remotel_to_keep):
    prune_epoch = (datetime.datetime.today() - datetime.timedelta(days=int(remote_keep_days))).strftime("%s")
    remote_snapshots = get_remote_snapshots(remote_dataset, remote_server)
    print("Enter pruning for remote/backup system")
    for item in (remote_snapshots):
        remote_snapshots = get_remote_snapshots(remote_dataset, remote_server)
        if len(remote_snapshots) >= int(minimal_remote_to_keep):
            snapshot_epoch = subprocess.check_output([ssh,remote_server, zfs, 'list', '-H', '-p', '-t', 'snap', item, '-o', 'creation']).decode('utf-8')
            if snapshot_epoch < prune_epoch:
                print("Pruning remote " + item)
                prune = subprocess.check_output([ssh, remote_server, zfs, 'destroy', item]).decode('utf-8')
#            else:
#                print("Finished pruning")
        else:
            print("No pruning since there are " + minimal_remote_to_keep + " or less snapshots")
            break


def main():
    backup()
    local_prune(local_keep_days, minimal_local_to_keep)
    remote_prune(remote_keep_days, minimal_remote_to_keep)

if __name__ == "__main__":
    main()
