# zfsbackup



## General
This script runs from a local server and send snapshot to a remote/backup server.  Local server need to have root  ssh keys to remote server.

## Backup part
1. Creates a local snapshot if none is present and sends initial to remote host
2. If local snapshot is present but remote doesn't exist, sends first snapshot to remote host
3. If there is only 1 snapshot in local, create a 2nd and send incremental to remote
4. If there are multiple snapshots in local, send incremental to remote until caught up

## Pruning
1. Takes 2 pruing condition
   a. prune anything more than minimum snapshots to keep AND
   b. prune anything old than x days
2. Will prune local and remote when both conditions are met. 

## Need to do
1. Create remote pruning function.
   hint: copy the local pruning function but execute with ssh to remote
